const fs = require("fs");
const config = require("./config.json");

const ips = fs.readFileSync("ips.txt").toString().replace(/, /g, "\n").split("\n");

// Remove trailing whitespace
if (ips[ips.length -1] == "") {
  ips.pop();
} 

let base = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ProxifierProfile version="101" platform="Windows" product_id="0" product_minver="310">
  <Options>
    <Resolve>
      <AutoModeDetection enabled="true" />
      <ViaProxy enabled="false">
        <TryLocalDnsFirst enabled="false" />
      </ViaProxy>
      <ExclusionList>%ComputerName%; localhost; *.local</ExclusionList>
    </Resolve>
    <Encryption mode="disabled" />
    <HttpProxiesSupport enabled="false" />
    <HandleDirectConnections enabled="false" />
    <ConnectionLoopDetection enabled="true" />
    <ProcessServices enabled="false" />
    <ProcessOtherUsers enabled="false" />
  </Options>
  <ProxyList>
    `

// ids start at 101 - add proxies    
for (let i = 0; i < ips.length; ++i) {
  base += `<Proxy id="${i + 101}" type="SOCKS5">
      <Address>${ips[i]}</Address>
      <Port>${config.port}</Port>
      <Options>48</Options>
      <Authentication enabled="true">
        <Username>${config.user}</Username>
        <Password>${config.pass}</Password>
      </Authentication>
    </Proxy>
  `
}

base += `</ProxyList>
  <ChainList />
  <RuleList>
    <Rule enabled="true">
      <Name>Localhost</Name>
      <Targets>localhost; 127.0.0.1; %ComputerName%</Targets>
      <Action type="Direct" />
    </Rule>
    `;

// Add rules
for (let i = 2; i < ips.length; ++i) {
  base += `<Rule enabled="true">
      <Name>${config.rules.name + i}</Name>
      <Applications>${config.rules.application + i + ".exe"}</Applications>
      <Action type="Proxy">${i + 99}</Action>
    </Rule>
  `
}

base += `</RuleList>
</ProxifierProfile>`    

fs.writeFile(config.outputfile + ".ppx", base, { flag: "wx" }, function(err) {
  if (err)
    console.log(err);

  console.log("Proxifier profile created successfully: " + config.outputfile);
});
